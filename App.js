import React, { Component } from 'react';
import { View } from 'react-native';
import { OTSession, OTPublisher, OTSubscriber } from 'opentok-react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.apiKey = '46623282';
    this.sessionId = '1_MX40NjYyMzI4Mn5-MTU4NTY2NzMyNzQwMn5DRkpwUnMyR2hEWkJmZ2cvNVZhZXpJaEJ-fg';
    this.token = 'T1==cGFydG5lcl9pZD00NjYyMzI4MiZzaWc9NjRmZTcyMzQ2ZmQ3Zjc3NmQ3ZDIyZDNiZDA1ZjEzOWE4MDUzMDEyOTpzZXNzaW9uX2lkPTFfTVg0ME5qWXlNekk0TW41LU1UVTROVFkyTnpNeU56UXdNbjVEUmtwd1VuTXlSMmhFV2tKbVoyY3ZOVlpoWlhwSmFFSi1mZyZjcmVhdGVfdGltZT0xNTg1NjY3MzQ0Jm5vbmNlPTAuODMzMDI5MzAxNzkzNDMyNyZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTg1NjcwOTQ0JmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9';
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <OTSession apiKey={this.apiKey} sessionId={this.sessionId} token={this.token}>
          <OTPublisher style={{ width: 100, height: 100 }} />
          <OTSubscriber style={{ width: 100, height: 100 }} />
        </OTSession>
      </View>
    );
  }
}
